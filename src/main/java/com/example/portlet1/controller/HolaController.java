package com.example.portlet1.controller;

import com.liferay.portletmvc4spring.bind.annotation.ResourceMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@RequestMapping("VIEW")
public class HolaController {
    private static final Logger logger = LoggerFactory.getLogger(HolaController.class);
    @RequestMapping
    String holaMundo() {
        return "hola-mundo";
    }

    @ResourceMapping("saludar")
    public void saludar() {
        logger.debug("Hola desde ResourceMapping");
    }
}