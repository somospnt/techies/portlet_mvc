<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<portlet:defineObjects/>

<portlet:resourceURL id="saludar" var="saludarUrl"/>

<div>
    <h1>Hola Mundo!</h1>
</div>
<script>
    $.ajax({
            url: "<%= saludarUrl %>",
            method: "GET",
            success: function (data) {
              console.log("Respuesta exitosa");
            },
          });
</script>
